/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package observer;

import java.util.ArrayList;

/**
 *
 * @author Zacharie
 */
public interface Observable {
    
    public void attach(Observer o);
    
    public void detach(Observer o);
    
    public void notifyObs();
}
