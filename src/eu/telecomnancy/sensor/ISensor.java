package eu.telecomnancy.sensor;

<<<<<<< HEAD
import observer.Observable;

public interface ISensor extends Observable{
=======
<<<<<<< HEAD
import observer.Observable;

public interface ISensor extends Observable{
=======
public interface ISensor {
>>>>>>> dbcc9431493f6ba555cc8716a8556feab005870a
>>>>>>> 4c02cc03be289da00aef98303a8687181290494c
    /**
     * Enable the sensor.
     */
    public void on();

    /**
     * Disable the sensor.
     */
    public void off();

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public boolean getStatus();

    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public void update() throws SensorNotActivatedException;

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public double getValue() throws SensorNotActivatedException;

}
