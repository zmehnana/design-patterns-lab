/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.util.ArrayList;
import java.util.Random;
import observer.Observer;

/**
 *
 * @author Zacharie
 */
public class SensorOff implements SensorState{
    private ArrayList<Observer> obs=new ArrayList<>();
    boolean state=false;
    double value = 0;

    @Override
    public void on() {
    }

    @Override
    public void off() {
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

    @Override
    public void attach(Observer o) {

        if(!obs.contains(o)){
            obs.add(o);
        }
    }

    @Override
    public void detach(Observer o) {
        if(obs.contains(o)){
            obs.remove(o);
        }
    }

    @Override
    public void notifyObs() {
        for(int i=0;i<obs.size();i++){
            obs.get(i).update();
        }
    }

    
}
