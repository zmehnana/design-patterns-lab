/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

import eu.telecomnancy.sensor.SensorNotActivatedException;
import observer.Observer;

/**
 *
 * @author Zacharie
 */
public class SensorUpdate {
    private SensorState sens;
    
    public SensorUpdate(SensorState s){
        sens=s;
    }
    
    public void on() {
        sens.on();
    }

    public void off() {
        sens.off();
    }

    public boolean getStatus() {
        return sens.getStatus();
    }

    public void update() throws SensorNotActivatedException {
        sens.update();
    }

    
    public double getValue() throws SensorNotActivatedException {
        return sens.getValue();
    }
    
    public void attach(Observer o) {
        sens.attach(o);
    }

    public void detach(Observer o) {
        sens.detach(o);
    }

    public void notifyObs() {
        sens.notifyObs();
    }
    
    
}
