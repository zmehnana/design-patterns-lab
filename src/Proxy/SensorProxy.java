/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proxy;
import java.util.Date;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import observer.Observer;

/**
 *
 * @author Zacharie
 */
public class SensorProxy implements ISensor{

    private ISensor sensor;
    private DateFormat dateFormat;
    
    public SensorProxy(ISensor s){
        sensor=s;
        dateFormat= new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    }
    @Override
    public void on() {
        System.out.println(dateFormat.format(new Date())+ "\tméthode: on()\tvaleur de retour: NB");
        sensor.on();
    }

    @Override
    public void off() {
        System.out.println(dateFormat.format(new Date())+ "\tméthode: off()\tvaleur de retour: NB");
        sensor.off();
    }

    @Override
    public boolean getStatus() {
        System.out.println(dateFormat.format(new Date())+ "\tméthode: getStatus()\tvaleur de retour: "+sensor.getStatus());
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        System.out.println(dateFormat.format(new Date())+ "\tméthode: update()\tvaleur de retour: NB");
        sensor.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        System.out.println(dateFormat.format(new Date())+ "\tméthode: getValue()\tvaleur de retour: "+sensor.getValue());
        return sensor.getValue();
    }

    @Override
    public void attach(Observer o) {
        System.out.println(dateFormat.format(new Date())+ "\tméthode: attach()\tvaleur de retour: NB");
        sensor.attach(o);
    }

    @Override
    public void detach(Observer o) {
        System.out.println(dateFormat.format(new Date())+ "\tméthode: detach()\tvaleur de retour: NB");
        sensor.detach(o);
    }

    @Override
    public void notifyObs() {
        System.out.println(dateFormat.format(new Date())+ "\tméthode: notify()\tvaleur de retour: NB");
        sensor.notify();
    }

    
}
