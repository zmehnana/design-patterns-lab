/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fabrique;

import decorateur.FahrenheitDecoration;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;

/**
 *
 * @author Zacharie
 */
public class DecoFahrenheitCreator extends Fabrique{
    @Override
    public ISensor create(){
        return new FahrenheitDecoration(new TemperatureSensor());
    }
}
