/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proxy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import observer.Observer;

/**
 *
 * @author Zacharie
 */
public class LegacyTemperatureSensorAdapteur implements ISensor{

    private LegacyTemperatureSensor lTSensor;
    
    public LegacyTemperatureSensorAdapteur(LegacyTemperatureSensor sensor){
        lTSensor=sensor;
    }
    public void on() {
        if(!this.lTSensor.getStatus()){
            this.lTSensor.onOff();
        }
        
    }

    @Override
    public void off() {
        if(this.lTSensor.getStatus()){
            this.lTSensor.onOff();
        }
    }

    @Override
    public boolean getStatus() {
        return this.lTSensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if(this.lTSensor.getStatus()){
            this.lTSensor.onOff();
            this.lTSensor.onOff();
        }
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return this.lTSensor.getTemperature();
    }

    @Override
    public void attach(Observer o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void detach(Observer o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void notifyObs() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
