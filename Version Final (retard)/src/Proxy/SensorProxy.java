/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Proxy;
import java.util.Date;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LogLevel;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.SimpleSensorLogger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import observer.Observer;

/**
 *
 * @author Zacharie
 */
public class SensorProxy implements ISensor{

    private ISensor sensor;
    private DateFormat dateFormat;
    private SimpleSensorLogger logger;
    
    public SensorProxy(ISensor s){
        sensor=s;
        dateFormat= new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        logger=new SimpleSensorLogger();
        
    }
    @Override
    public void on() {
        logger.log(LogLevel.INFO, dateFormat.format(new Date())+ "\tméthode: on()\tvaleur de retour: NB");
        sensor.on();
    }

    @Override
    public void off() {
        logger.log(LogLevel.INFO, dateFormat.format(new Date())+ "\tméthode: off()\tvaleur de retour: NB");
        sensor.off();
    }

    @Override
    public boolean getStatus() {
        logger.log(LogLevel.INFO, dateFormat.format(new Date())+ "\tméthode: getStatus()\tvaleur de retour: "+sensor.getStatus());
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        logger.log(LogLevel.INFO, dateFormat.format(new Date())+ "\tméthode: update()\tvaleur de retour: NB");
        sensor.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        try {
           logger.log(LogLevel.INFO, dateFormat.format(new Date())+ "\tméthode: getValue()\tvaleur de retour: "+sensor.getValue());
            
        } catch (SensorNotActivatedException ex) {
            logger.log(LogLevel.ERROR, dateFormat.format(new Date())+ "\tméthode: getValue()\tErreur: "+ex);
            Logger.getLogger(SensorProxy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sensor.getValue();
    }

    @Override
    public void attach(Observer o) {
        logger.log(LogLevel.INFO, dateFormat.format(new Date())+ "\tméthode: attach()\tvaleur de retour: NB");
        sensor.attach(o);
    }

    @Override
    public void detach(Observer o) {
        logger.log(LogLevel.INFO, dateFormat.format(new Date())+ "\tméthode: detach()\tvaleur de retour: NB");
        sensor.detach(o);
    }

    @Override
    public void notifyObs() {
        logger.log(LogLevel.INFO, dateFormat.format(new Date())+ "\tméthode: notifyObs()\tvaleur de retour: NB");
        sensor.notify();
    }

    
}
