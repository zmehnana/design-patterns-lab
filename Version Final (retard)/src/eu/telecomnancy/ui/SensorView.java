package eu.telecomnancy.ui;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import command.CommandOff;
import command.CommandOn;
import command.CommandUpdate;
import decorateur.ArronditDecoration;
import decorateur.FahrenheitDecoration;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import observer.Observer;

public class SensorView extends JPanel implements Observer{
    private ISensor sensor;

    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton fahrenheit = new JButton("Fahrenheit");
    private JButton arrondit = new JButton("Round");
    
    private ArronditDecoration arronditSens;
    private FahrenheitDecoration fhSens;
    
    private CommandUpdate updateC;
    private CommandOn onC;
    private CommandOff offC;

    public SensorView(ISensor c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());
        arronditSens=new ArronditDecoration(this.sensor);
        fhSens=new FahrenheitDecoration(this.sensor);
        
        updateC=new CommandUpdate(sensor);
        onC=new CommandOn(sensor);
        offC=new CommandOff(sensor);

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                sensor.on();
                onC.execute();
                try {
                    value.setText("Température: "+String.valueOf(sensor.getValue())+"°C");
                } catch (SensorNotActivatedException ex) {
                    Logger.getLogger(SensorView.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                
            }
        });

        arrondit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    value.setText("Température: "+String.valueOf(arronditSens.getValue())+"°C");
                } catch (SensorNotActivatedException ex) {
                    Logger.getLogger(SensorView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        fahrenheit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    value.setText("Température: "+String.valueOf(fhSens.getValue())+"°F");
                } catch (SensorNotActivatedException ex) {
                    Logger.getLogger(SensorView.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //sensor.off();
                offC.execute();
                value.setText("OFF");
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                try {
//                    sensor.update();
//                } catch (SensorNotActivatedException sensorNotActivatedException) {
//                    sensorNotActivatedException.printStackTrace();
//                }
                updateC.execute();
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(arrondit);
        buttonsPanel.add(fahrenheit);

        this.add(buttonsPanel, BorderLayout.SOUTH);
        
        this.sensor.attach(this);
    }

    @Override
    public void update() {
        try {
            value.setText("Température: "+String.valueOf(this.sensor.getValue())+"°C");
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(SensorView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
