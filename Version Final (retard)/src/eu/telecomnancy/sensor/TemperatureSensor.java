package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Random;
import observer.Observable;
import observer.Observer;

public class TemperatureSensor implements ISensor{
    private ArrayList<Observer> obs=new ArrayList<>();
    boolean state;
    double value = 0;

    @Override
    public void on() {
        state = true;
    }

    @Override
    public void off() {
        state = false;
    }

    @Override
    public boolean getStatus() {
        return state;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state)
            value = (new Random()).nextDouble() * 100;
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        
        this.notifyObs();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

    @Override
    public void attach(Observer o) {

        if(!obs.contains(o)){
            obs.add(o);
        }
    }

    @Override
    public void detach(Observer o) {
        if(obs.contains(o)){
            obs.remove(o);
        }
    }

    @Override
    public void notifyObs() {
        for(int i=0;i<obs.size();i++){
            obs.get(i).update();
        }
    }

}
