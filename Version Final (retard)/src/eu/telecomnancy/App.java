package eu.telecomnancy;

import Proxy.SensorProxy;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App {

    public static void main(String[] args) {
        ISensor sensor = new TemperatureSensor();
        SensorProxy sp=new SensorProxy(sensor);
        new ConsoleUI(sp);
    }

}