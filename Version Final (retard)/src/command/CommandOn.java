/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

import eu.telecomnancy.sensor.ISensor;

/**
 *
 * @author Zacharie
 */
public class CommandOn  implements InterfaceCommand{

    private ISensor sensor;
    
    public CommandOn(ISensor s){
        sensor=s;
    }
   
    
    public void execute() {
        sensor.on();
    
    }
    
}
