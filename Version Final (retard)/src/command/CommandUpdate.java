/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zacharie
 */
public class CommandUpdate implements InterfaceCommand{

    private ISensor sensor;
    
    public CommandUpdate(ISensor s){
        sensor=s;
    }
    
    public void execute() {
        try {
            sensor.update();
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(CommandUpdate.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
