/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorateur;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import observer.Observer;

/**
 *
 * @author Zacharie
 */
public abstract class AbstractDecorateur implements ISensor{
    protected ISensor sensor;
    
    protected AbstractDecorateur(ISensor s){
        sensor=s;
    }

    @Override
    public void on() {
        sensor.on();
    }

    @Override
    public void off() {
        sensor.off();
    }

    @Override
    public boolean getStatus() {
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        sensor.update();
    }

    

    @Override
    public void attach(Observer o) {
        sensor.attach(o);
    }

    @Override
    public void detach(Observer o) {
        sensor.detach(o);
    }

    @Override
    public void notifyObs() {
        sensor.notifyObs();
    }
}
