/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorateur;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 *
 * @author Zacharie
 */
public class FahrenheitDecoration extends AbstractDecorateur{

    public FahrenheitDecoration(ISensor sensor) {
        super(sensor);
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (sensor.getStatus())
            return 1.8*sensor.getValue()+32;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");   
    
    }
    
    
}
