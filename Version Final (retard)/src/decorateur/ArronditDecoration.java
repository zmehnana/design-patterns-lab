/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorateur;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import observer.Observer;

/**
 *
 * @author Zacharie
 */
public class ArronditDecoration extends AbstractDecorateur {

    public ArronditDecoration(ISensor sensor) {
        super(sensor);
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (sensor.getStatus())
            return Math.round(sensor.getValue());
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");   
    
    }

    
    
}
